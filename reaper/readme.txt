Descripció:

	Reaper és un joc per a dos jugadors on un dels dos ha de sobreviure per a guanyar el joc, mentre que el jugador que mor ho perd. 
	Durant la partida, hauràn de disparar als enemics per a tractar de no morir mentre aconsegueixen mantenir-se a la màxima quantitat
	de Punts de Salut possibles, agafant totes les vides que puguin durant el nivell 1, les quals aniràn sortint a posicions aleatòries
	cada cop que algú les agafi, donant un punt de salut extra. Si un enemic xoca amb un jugador, el jugador perdrà punts de salut.
	Acaba amb els enemics i esquivals per a sobreviure!

	Si un dels dos jugadors aconsegueix arribar a 10 Punts de Salut, començarà el nivell 2, on ja no serà possible aconseguir més 
	vides extra i només el temps i l'habilitat dels jugadors decidirà qui dels dos morirà i qui dels dos surtirà guanyador d'aquesta
	lluita per la supervivència.

	El projectil que disparen es una guadanya boomerang que segueix l'altura del jugador i després torna amb ell.

Controls:

	Al menú i game over:

		·START per a jugar
		·EXIT per a tancar el joc

	En partida:

		·Jugador 1 (groc):

			-Fletxa esquerra (moure's cap a l'esquerra)
			-Fletxa dreta (moure's cap a la dreta)
			-Fletxa amunt (moure's cap amunt)
			-Fletxa avall (moure's cap avall)
			-P (disparar)

		·Jugador 2 (vermell):

			-A (moure's cap a l'esquerra)
			-D (moure's cap a la dreta)
			-W (moure's cap amunt)
			-S (moure's cap avall)
			-Espai (disparar)

Enllaç a gitlab: 
	
	https://gitlab.com/2L_Alejandro/reaper