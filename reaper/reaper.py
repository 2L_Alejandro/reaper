import pygame
import pygame.freetype
import random

FPS=60
WINDOW_SIZE_X=800
WINDOW_SIZE_Y=600
PLAYER_VEL=0.2
SCYTHE_VEL=6
ENEMY_VEL=4
SPAWN_ENEMY_TIME=1000

GAME_STATE_MENU=1
GAME_STATE_PLAYING=2
GAME_STATE_RESULTS=3
GAME_STATE_EXIT=4

def collisions(players, lifeup, enemies, scythes1, scythes2):
    for n in range(len(enemies)-1, -1, -1):
        for m in range(len(scythes1)-1, -1, -1):
            enemy=enemies[n]
            enemy_square=enemy['sprites'].get_rect().move(enemy['x'], enemy['y'])
            scythesprite=scythes1[m]['sprites']
            scythe_square=scythesprite.get_rect().move(scythes1[m]['x'], scythes1[m]['y'])
            if scythe_square.colliderect(enemy_square):
                del enemies[n]
    for n in range(len(enemies)-1, -1, -1):
        for m in range(len(scythes2)-1, -1, -1):
            enemy=enemies[n]
            enemy_square=enemy['sprites'].get_rect().move(enemy['x'], enemy['y'])
            scythesprite=scythes2[m]['sprites']
            scythe_square=scythesprite.get_rect().move(scythes2[m]['x'], scythes2[m]['y'])
            if scythe_square.colliderect(enemy_square):
                del enemies[n]            
    for player in players:
        playersprite=player['sprites']
        player_square=playersprite.get_rect().move(player['x'], player['y'])
        if len(lifeup)>0:
            life=lifeup[0]
            life_square=life['sprite'].get_rect().move(life['x'], life['y'])
            if player_square.colliderect(life_square):
                player['hp']+=1
                life['x']=random.randint(0, WINDOW_SIZE_X-life['sprite'].get_width())
                life['y']=random.randint(0, WINDOW_SIZE_Y-life['sprite'].get_height())
                if player['hp']>10:
                    player['hp']=10
                if player['hp']==10 and player==players[0] or player['hp']==10 and player==players[1]:
                    lifeup.remove(life)
        for n in range(len(enemies)-1, -1, -1):
            enemy=enemies[n]
            enemy_square=enemy['sprites'].get_rect().move(enemy['x'], enemy['y'])
            if player_square.colliderect(enemy_square):
                player['hp']-=1
                del enemies[n]

def spawn_enemy(enemy_type):
    posicio=random.choice(['-150','850'])
    enemyx=int(posicio)
    enemy={
        'sprites': enemy_type['sprites'],
        'facing': enemy_type['facing'],
        'x': enemyx,
        'y': random.randint(0, WINDOW_SIZE_Y-enemy_type['sprites'].get_height())
    }
    if enemy['x']<400:
        enemy['sprites']=pygame.image.load('images/enemy/enemy_right.xcf')
        enemy['facing']='right'
    else:
        enemy['sprites']=pygame.image.load('images/enemy/enemy_left.xcf')
        enemy['facing']='left'
    return enemy
    
def move_player(players, delta, scythes1, scythes2):
    moved=False
    vel=int(PLAYER_VEL*delta)
    keys = pygame.key.get_pressed()
    for player in players:
        if player==players[0]:
            if len(scythes1)==0:
                if moved==False and player['facing']=='left':
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_idle.xcf')
                if moved==False and player['facing']=='right':
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_idle.xcf')
                if keys[player['keys']['left']] and player['x']>0:
                    player['x']=max(player['x']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_move.xcf')
                    moved=True
                if keys[player['keys']['right']] and player['x']<WINDOW_SIZE_X-25:
                    player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_move.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='left':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_idle.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='right':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_idle.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='left':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_idle.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='right':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_idle.xcf')
                    moved=True
            else:
                if moved==False and player['facing']=='left':
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_attack.xcf')
                if moved==False and player['facing']=='right':
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_attack.xcf')
                if keys[player['keys']['left']] and player['x']>0:
                    player['x']=max(player['x']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_attack.xcf')
                    moved=True
                if keys[player['keys']['right']] and player['x']<WINDOW_SIZE_X-25:
                    player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_attack.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='left':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_attack.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='right':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_attack.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='left':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/yellow_left_attack.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='right':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/yellow_right_attack.xcf')
                    moved=True
        if player==players[1]:
            if len(scythes2)==0:
                if moved==False and player['facing']=='left':
                    player['sprites']=pygame.image.load('images/reaper/red_left_idle.xcf')
                if moved==False and player['facing']=='right':
                    player['sprites']=pygame.image.load('images/reaper/red_right_idle.xcf')
                if keys[player['keys']['left']] and player['x']>0:
                    player['x']=max(player['x']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/red_left_move.xcf')
                    moved=True
                if keys[player['keys']['right']] and player['x']<WINDOW_SIZE_X-25:
                    player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/red_right_move.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='left':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/red_left_idle.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='right':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/red_right_idle.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='left':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/red_left_idle.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='right':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/red_right_idle.xcf')
                    moved=True
            else:
                if moved==False and player['facing']=='left':
                    player['sprites']=pygame.image.load('images/reaper/red_left_attack.xcf')
                if moved==False and player['facing']=='right':
                    player['sprites']=pygame.image.load('images/reaper/red_right_attack.xcf')
                if keys[player['keys']['left']] and player['x']>0:
                    player['x']=max(player['x']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/red_left_attack.xcf')
                    moved=True
                if keys[player['keys']['right']] and player['x']<WINDOW_SIZE_X-25:
                    player['x']=min(player['x']+vel, WINDOW_SIZE_X-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/red_right_attack.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='left':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/red_left_attack.xcf')
                    moved=True
                if keys[player['keys']['up']] and player['y']>0 and player['facing']=='right':
                    player['y']=max(player['y']-vel, 0)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/red_right_attack.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='left':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='left'
                    player['sprites']=pygame.image.load('images/reaper/red_left_attack.xcf')
                    moved=True
                if keys[player['keys']['down']] and player['y']<WINDOW_SIZE_Y-25 and player['facing']=='right':
                    player['y']=min(player['y']+vel, WINDOW_SIZE_Y-25)
                    player['facing']='right'
                    player['sprites']=pygame.image.load('images/reaper/red_right_attack.xcf')
                    moved=True

def draw_player(screen, players):
    for player in players:
        sprite=player['sprites']
        square=sprite.get_rect().move(player['x'], player['y'])
        screen.blit(sprite, square)

def draw_scythe1(screen, scythes):
    for scythe in scythes:
        sprite=scythe['sprites']
        square=sprite.get_rect().move(scythe['x'], scythe['y'])
        screen.blit(sprite, square)

def draw_scythe2(screen, scythes):
    for scythe in scythes:
        sprite=scythe['sprites']
        square=sprite.get_rect().move(scythe['x'], scythe['y'])
        screen.blit(sprite, square)

def draw_lives(screen, lifeup):
    if len(lifeup)!=0:
        life=lifeup[0]
        sprite=life['sprite']
        square=sprite.get_rect().move(life['x'], life['y'])
        screen.blit(sprite, square)

def draw_enemies(screen, enemies):
    for enemy in enemies:
        sprite=enemy['sprites']
        square=sprite.get_rect().move(enemy['x'], enemy['y'])
        screen.blit(sprite, square)

def create_lifeup():
    lifeup=[{
        'sprite': pygame.image.load('images/reaper/lifeup.xcf'),
        'time': 10000,
    }]
    life=lifeup[0]
    life['x']=random.randint(0, WINDOW_SIZE_X-life['sprite'].get_width())
    life['y']=random.randint(0, WINDOW_SIZE_Y-life['sprite'].get_height())
    return lifeup

def create_scythe():
    scythe={
        'sprites': pygame.image.load('images/reaper/attack.xcf')
    }
    return scythe

def create_enemy():
    enemy={
        'sprites': pygame.image.load('images/enemy/enemy_left.xcf'),
        'facing': 'left'
    }
    return enemy

def create_players():
    prefix=['yellow_', 'red_']
    sufix=['_idle', '_attack', '_move']
    facing=['right', 'left']
    keys=[
        {
            'right': pygame.K_RIGHT,
            'left': pygame.K_LEFT,
            'up': pygame.K_UP,
            'down': pygame.K_DOWN,
        },
        {
            'right': pygame.K_d,
            'left': pygame.K_a,
            'up': pygame.K_w,
            'down': pygame.K_s
        }
    ]
    x=[500,300]
    players=[]
    for n_player in range(0,2):
        player={
            'x':x[n_player]-27,
            'y':300-15,
            'hp':5,
            'facing':facing[n_player],
            'keys': keys[n_player],
            'sprites': pygame.image.load('images/reaper/'+prefix[n_player]+facing[n_player]+sufix[0]+'.xcf')
        }
        players.append(player)
    return players

def draw_overlay(screen, font, players, lifeup):
    mode=1
    if players[0]['hp']!=10 and players[1]['hp']!=10:
        mode=1
    if players[0]['hp']==10 or players[1]['hp']==10:
        mode=2
    font.render_to(screen, (550, 550), 'Vida (J2): '+str(players[1]['hp'])+' PS')
    font.render_to(screen, (25, 550), 'Vida (J1): '+str(players[0]['hp'])+' PS')
    if mode==1 and len(lifeup)>0:
        font.render_to(screen, (25, 25), 'Nivell 1: aconsegueix vida!')
    else:
        font.render_to(screen, (25, 25), 'Nivell 2: sobreviu!')

def game_over(screen, font, players):
    start_btn=pygame.image.load('images/menu/start.xcf')
    exit_btn=pygame.image.load('images/menu/exit.xcf')
    going=True
    if players[0]['hp']<=0 and players[1]['hp']<=0:
        font.render_to(screen, (200, 225), 'Han mort els dos jugadors...')
        font.render_to(screen, (25, 325), '(Premeu START per a tornar a jugar; EXIT per a sortir)')
    else:
        if players[0]['hp']<=0:
            font.render_to(screen, (250, 125), 'Ha mort el jugador 1.')
            font.render_to(screen, (200, 225), 'La victòria és del jugador 2!')
            font.render_to(screen, (25, 325), '(Premeu START per a tornar a jugar; EXIT per a sortir)')
        if players[1]['hp']<=0:
            font.render_to(screen, (250, 125), 'Ha mort el jugador 2.')
            font.render_to(screen, (200, 225), 'La victòria és del jugador 1!')
            font.render_to(screen, (25, 325), '(Premeu START per a tornar a jugar; EXIT per a sortir)')
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=start_btn.get_rect().move(50,450)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_PLAYING
                square=exit_btn.get_rect().move(475,450)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_EXIT
        screen.blit(start_btn, start_btn.get_rect().move(50,450))
        screen.blit(exit_btn, exit_btn.get_rect().move(475,450))
        pygame.display.flip()
    return result

def game_playing(screen):
    overlay_font = pygame.freetype.Font("fonts/Lato-Black.ttf", 32)
    players=create_players()
    enemy=create_enemy()
    enemies=[]
    lifeup=create_lifeup()
    scythes1=[]
    scythes2=[]
    clock = pygame.time.Clock()
    elapsed_time=0
    going=True
    while going:
        delta=clock.tick(FPS)
        elapsed_time+=delta
        for event in pygame.event.get():
            for player in players:
                if player['hp']<=0 and player==players[0] or player['hp']<=0 and player==players[1]:
                    going=False
                    result=GAME_STATE_RESULTS
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            elif event.type==pygame.KEYDOWN:
                for player in players:
                    if player==players[0]:
                        if event.key==pygame.K_p and len(scythes1)==0:
                            scythes1.append(create_scythe())
                            scythe=scythes1[0]
                            scythe['x']=player['x']
                            scythe['y']=player['y']
                            scythe['facing']=player['facing']
                            scythe['going']=True
                    if player==players[1]:
                        if event.key==pygame.K_SPACE and len(scythes2)==0:
                            scythes2.append(create_scythe())
                            scythe=scythes2[0]
                            scythe['x']=player['x']
                            scythe['y']=player['y']
                            scythe['facing']=player['facing']
                            scythe['going']=True
        if len(scythes1)>0:
            for scythe in scythes1:
                if scythe['going']==True:
                    if scythe['facing']=='left':
                        scythe['x']=scythe['x']-SCYTHE_VEL
                    if scythe['x']<=0:
                        scythe['going']=False
                    if scythe['facing']=='right':
                        scythe['x']=scythe['x']+SCYTHE_VEL
                    if scythe['x']>=WINDOW_SIZE_X:
                        scythe['going']=False
                elif scythe['x']>players[0]['x'] and scythe['going']==False and scythe['facing']=='left':
                    scythes1.remove(scythe)
                elif scythe['x']<players[0]['x'] and scythe['going']==False and scythe['facing']=='right':
                    scythes1.remove(scythe)
                else:
                    if scythe['facing']=='left':
                        scythe['x']=scythe['x']+SCYTHE_VEL
                    if scythe['facing']=='right':
                        scythe['x']=scythe['x']-SCYTHE_VEL
                scythe['y']=players[0]['y']
        if len(scythes2)>0:
            for scythe in scythes2:
                if scythe['going']==True:
                    if scythe['facing']=='left':
                        scythe['x']=scythe['x']-SCYTHE_VEL
                    if scythe['x']<=0:
                        scythe['going']=False
                    if scythe['facing']=='right':
                        scythe['x']=scythe['x']+SCYTHE_VEL
                    if scythe['x']>=WINDOW_SIZE_X:
                        scythe['going']=False
                elif scythe['x']>players[1]['x'] and scythe['going']==False and scythe['facing']=='left':
                    scythes2.remove(scythe)
                elif scythe['x']<players[1]['x'] and scythe['going']==False and scythe['facing']=='right':
                    scythes2.remove(scythe)
                else:
                    if scythe['facing']=='left':
                        scythe['x']=scythe['x']+SCYTHE_VEL
                    if scythe['facing']=='right':
                        scythe['x']=scythe['x']-SCYTHE_VEL
                scythe['y']=players[1]['y']
        screen.fill((128,128,224))
        if elapsed_time>SPAWN_ENEMY_TIME:
            elapsed_time-=SPAWN_ENEMY_TIME
            for i in range(4):
                enemies.append(spawn_enemy(enemy))
        for enemy_move in enemies:
            if enemy_move['x']==-200 or enemy_move['x']==WINDOW_SIZE_X+100:
                enemies.remove(enemy_move)
            if enemy_move['facing']=='left':
                enemy_move['x']=enemy_move['x']-ENEMY_VEL
            if enemy_move['facing']=='right':
                enemy_move['x']=enemy_move['x']+ENEMY_VEL
        move_player(players, delta, scythes1, scythes2)
        collisions(players, lifeup, enemies, scythes1, scythes2)
        draw_lives(screen, lifeup)
        draw_enemies(screen, enemies)
        draw_scythe1(screen, scythes1)
        draw_scythe2(screen, scythes2)
        draw_player(screen, players)
        draw_overlay(screen, overlay_font, players, lifeup)
        pygame.display.flip()
    return result, overlay_font, players

def game_menu(screen):
    start_btn=pygame.image.load('images/menu/start.xcf')
    exit_btn=pygame.image.load('images/menu/exit.xcf')
    background=pygame.image.load('images/menu/background.jpg')
    going=True
    while going:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                going=False
                result=GAME_STATE_EXIT
            if event.type == pygame.MOUSEBUTTONDOWN:
                square=start_btn.get_rect().move(400, 80)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_PLAYING
                square=exit_btn.get_rect().move(60, 400)
                if square.collidepoint(pygame.mouse.get_pos()):
                    going=False
                    result=GAME_STATE_EXIT
        screen.blit(background, background.get_rect())
        screen.blit(start_btn, start_btn.get_rect().move(400,80))
        screen.blit(exit_btn, exit_btn.get_rect().move(60,400))
        pygame.display.flip()
    return result

def main():
    pygame.init()
    
    screen = pygame.display.set_mode([WINDOW_SIZE_X, WINDOW_SIZE_Y])
    game_icon = pygame.image.load('images/icon.png')
    
    pygame.display.set_caption('Reaper')
    pygame.display.set_icon(game_icon)

    game_state=GAME_STATE_MENU
    while game_state!=GAME_STATE_EXIT:
        if game_state==GAME_STATE_MENU:
            game_state=game_menu(screen)
        elif game_state==GAME_STATE_PLAYING:
            game_state, font, players=game_playing(screen)
        elif game_state==GAME_STATE_RESULTS:
            game_state=game_over(screen, font, players)
    pygame.quit()

main()